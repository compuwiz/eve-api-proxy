<?php
ini_set("display_errors", 0);

//
// eproxy.php
// Version 0.3.0
//
// Requires: PHP5, mysql, curl, (optionally) mcrypt
//
// ======== INSTRUCTIONS =========
// 1: Place eproxy.php in /.api/
// 2: .htaccess settings (place in main directory)
// RewriteEngine On
// RewriteBase /
// RewriteRule ^(api|server|map|eve|account|char|corp)/(.*).xml.aspx$  /.api/index.php?/anon/$1/$2.xml.aspx
// RewriteRule ^api/(.*).xsl.aspx$  /.api/index.php?/anon/api/$1.xsl.aspx//
// RewriteRule ^(.*)/(api|server|map|eve|account|char|corp)/(.*).xml.aspx$  /.api/index.php?/$1/$2/$3.xml.aspx
// RewriteRule ^(.*)/api/(.*).xsl.aspx$  /.api/index.php?/$1/api/$2.xsl.aspx
//
// BUILT IN ANALYTICS: (anonymous stats) /api/Stats.xml.aspx
//                     (user stats) /username/api/Stats.xml.aspx
//
//
// Simple caching proxy for EVE API data.  Just replace
// http://api.eve-online.com with
// http://your.proxy.location/eproxy.php and it should work
// transparently.  Final URLs should look like:
// http://your.proxy.location/eproxy.php/account/Characters.xml.aspx
// or (username = anything you want [letters and numbers only] for a username for analytic purposes. i used to use ip but this takes care of the dynamic ip issue).
// http://your.proxy.location/eproxy.php/username/account/Characters.xml.aspx
// Your API key is hashed and used as an index; your key is never
// stored in the databse. If I get un-lazy, I'll encrypt the stored
// data with your key as well, ensuring that even in the unlikely case
// of database compromise, noone will be able to see your data.
// 
// Note that this ignores the cachedUntil return from API methods, and
// instead always caches entries for some fixed amount of time from
// when the last request was, per API call.  See the data structures
// below for the amount of time each API method is cached.  Also, not
// all API methods are here; just add the ones that are missing to the
// arrays.
//
// Configuration options are after this comment; at a minimum you'll
// need to specify a database connection string and set up the
// database.
//
// Send ISK to Controller Vrelk (major updates) and/or Isidien Madcap (original code) if you get any use out of this!
//
// Do whatever the hell you want BSD license follows.
//
// Copyright (C) 2007 Mr. Rupert's Quality Software Group
// Copyright (C) 2007 Vladimir Vukicevic
//
// Permission to use, copy, modify, distribute, and sell this software
// and its documentation for any purpose is hereby granted without
// fee, provided that the above copyright notice appear in all copies
// and that both that copyright notice and this permission notice
// appear in supporting documentation, and that the name of
// the author not be used in advertising or publicity pertaining to
// distribution of the software without specific, written prior
// permission. The author makes no representations about the
// suitability of this software for any purpose.  It is provided "as
// is" without express or implied warranty.
//
// THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
// SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS, IN NO EVENT SHALL THE AUTHOR. BE LIABLE FOR ANY SPECIAL,
// INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
// OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
// IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//

//
// Configuration options
//

// whether to encrypt the data in the database; the api key will be used
// as the encryption key; if this is true, mcrypt is required
$do_crypt = true;

// database connection info

$db_server = "localhost";
$db_user = "username";
$db_password = "password";
$db_name = "eveapicache";

//enable / disable analytics
$enableanalytics = TRUE;

//
// End of Configuration
//

// we're generally going to send XML
header("Content-type: text/xml");
// we are a-ok with the cross-site awesomeness
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST");
header("Access-Control-Max-Age: 10000");
header("Cache-Control: no-cache");
header("X-CACHE-ADMIN: EPROXY by Controller Vrelk");

$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
  // this is xhr preflight; only headers matter
  exit();
}

// do gzip, if the accept-encoding sends the right thing
ob_start("ob_gzhandler");

//
// Database functions
//

// Should you need a non-PDO database, you can implement a new dbtype
// here.  Just implement the three functions with the specified semantics.
// If you're using PDO, you may need to modify the database schema to
// be compatible with your particular database:
//
// The 'date' field needs to be an exact numeric type; it will store
// seconds since the epoch in UTC.  'key' will be exactly 64 ascii
// bytes, and 'apicall' will be a variable number of characters but
// shouldn't exceed 64.  'value' will be a chunk of text, either the
// raw results or a base64-encoded version of the encrypted data if
// encryption is active (it will be text in both cases; it may contain
// UTF8 characters, so won't necessarily always be ASCII).


function eproxy_db_connect() {
    global $db;
    global $db_server, $db_user, $db_password;
    global $db_name;

    if (!$db_name) {
        print_error("mysql_db_name must be specified");
        exit();
    }

    $db = mysql_connect($db_server, $db_user, $db_password);
    if (!$db) {
        print_error("couldn't connect to mysql database");
        exit();
    }

    if (!mysql_select_db($db_name)) {
        print_error("couldn't select database");
        exit();
    }

    $q = mysql_query("SHOW TABLES LIKE 'data_cache'");
    if ($q == false || mysql_num_rows($q) == 0) {
        mysql_query("CREATE TABLE IF NOT EXISTS data_cache (apicall VARCHAR(64), kval CHAR(64), dval INTEGER, value TEXT)");
        mysql_query("CREATE INDEX data_key_index ON data_cache(kval)");
        mysql_query("CREATE TABLE IF NOT EXISTS `accessLog` (`id` int(11) NOT NULL AUTO_INCREMENT, `ip` varchar(128) NOT NULL, `method` varchar(128) NOT NULL, `hash` varchar(256) NOT NULL, `accessed` bigint(20) NOT NULL DEFAULT '0', PRIMARY KEY (`id`), UNIQUE KEY `hash` (`hash`), KEY `ip` (`ip`), KEY `method` (`method`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
    }

    if ($q != false)
        mysql_free_result($q);
}

function eproxy_db_query($ac, $k) {
    global $APICacheTimer;
    $d = time() - $APICacheTimer[$ac];
    $q = mysql_query("SELECT dval, value FROM data_cache WHERE apicall = '$ac' AND kval = '$k' AND dval > $d");
    $res = null;
    if ($q != false && mysql_num_rows($q) > 0) {
        $row = mysql_fetch_assoc($q);
        $res = $row['value'];
    }

    if ($q != false)
        mysql_free_result($q);

    return $res;
}

function eproxy_db_insert($ac, $k, $value) {
    $d = time();
    mysql_query("DELETE FROM data_cache where apicall = '$ac' AND kval = '$k'");
    mysql_query("INSERT INTO data_cache (apicall, kval, dval, value) VALUES('$ac', '$k', $d, '" . mysql_real_escape_string($value) . "')");
}
function eproxy_db_accessed($ac, $len, $user) {
    //$ip = $_SERVER["REMOTE_ADDR"];
    $ip = $user;
    $hash = md5($ip.$ac);
    mysql_query("INSERT INTO accessLog (`ip`, `method`, `hash`, `accessed`, `size`) VALUES ('" . mysql_real_escape_string($ip) . "', '" . mysql_real_escape_string($ac) . "', '" . mysql_real_escape_string($hash) . "', '1', '" . mysql_real_escape_string($len) . "') ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), `accessed`=`accessed`+1, `size`=`size`+" . mysql_real_escape_string($len) . ";");
}
function eproxy_db_accessed_get($ip) {
    $result = mysql_query("SELECT `method`, `accessed`, `size` FROM accessLog WHERE `ip`= '" . mysql_real_escape_string($ip) . "';");
    if(!$result){
      return array();
    }else{
      $results = array();
      while ($row = mysql_fetch_assoc($result)) {
           $results[] = $row;
      }
      mysql_free_result($result);
      return $results;
    }
}

function custom_call_sort($a,$b) {
     return $a['method']>$b['method'];
}

function bytesToSize($bytes, $precision = 2)
{  
    $kilobyte = 1024;
    $megabyte = $kilobyte * 1024;
    $gigabyte = $megabyte * 1024;
    $terabyte = $gigabyte * 1024;
   
    if (($bytes >= 0) && ($bytes < $kilobyte)) {
        return $bytes . '&#160;&#160;B';
 
    } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
        return round($bytes / $kilobyte, $precision) . '&#160;KB';
 
    } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
        return round($bytes / $megabyte, $precision) . '&#160;MB';
 
    } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
        return round($bytes / $gigabyte, $precision) . '&#160;GB';
 
    } elseif ($bytes >= $terabyte) {
        return round($bytes / $terabyte, $precision) . '&#160;TB';
    } else {
        return $bytes . '&#160;&#160;B';
    }
}

function getXSL($call, $user){
     if(stripos($call, "api/Stats.xsl.aspx") !== FALSE){
          header("Content-type: text/xsl");
          //$ip = $_SERVER["REMOTE_ADDR"];
          $ip = $user;
          echo <<<EOT
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
<head>
<title>EVE API Call Stats</title>
<style type="text/css">
body
{
font-family: verdana,helvetica,arial,sans-serif;
background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAAd0SU1FB9sDFAEjBeP3FY0AAAA9SURBVBjTfc4xDgAgCEPRLzP3vwzX6q4bGhXYmtc0jIiYHCfpjFiH7r4LP8yFCgGsw+eHGyXtwg9zoUKABZJzGxy/SzWpAAAAAElFTkSuQmCC);
}
table, th, td{
border: 1px solid black;
border-collapse: collapse;
padding-left: 10px;
padding-right: 10px;
background: #C0C0C0;
}
.method
{
display:block;
font-weight:bold;
}
.accessed
{
display: block;
color: #636363;
font-size: small;
font-style: italic;
}
.bw
{
font-family: "courier new",courier,monospace;
}
.aright
{
text-align: right;
}
.totals
{
background: #000000;
color: #FFFFFF;
}
</style>
</head>
<body>
<h2>EVE API Call Stats for: <u>$ip</u></h2>
<table>
<thead>
<tr><th>Method</th><th>Access Count</th><th>Approx Bandwidth</th></tr>
</thead>
<tbody>
  <xsl:for-each select="apistats/apistat">
      <tr>
        <td><xsl:value-of select="method" /></td>
        <td class="aright bw"><xsl:value-of select="accessed" /></td>
        <td class="aright bw"><xsl:value-of select="size" /></td>
      </tr>
  </xsl:for-each>
  <tr>
    <td class="totals aright">Total</td>
    <td class="totals aright bw"><xsl:value-of select="apistats/tcall" /></td>
    <td class="totals aright bw"><xsl:value-of select="apistats/tbw" /></td>
  </tr>
</tbody>
</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
EOT;
          exit();
     }elseif(stripos($call, "api/Stats.xml.aspx") !== FALSE){
          eproxy_db_connect();
          //$results = eproxy_db_accessed_get($_SERVER["REMOTE_ADDR"]);
          $results = eproxy_db_accessed_get($user);
          usort($results, "custom_call_sort");
          echo <<<EOT
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?xml-stylesheet type="text/xsl" href="/$user/api/Stats.xsl.aspx"?>
<apistats>
EOT;
$tsize = 0;
$tacc = 0;
foreach($results as $row){
$tsize += $row['size'];
$tacc += $row['accessed'];
echo "<apistat>\n";
echo "<method>".$row['method']."</method>\n";
echo "<accessed>".number_format($row['accessed'])."</accessed>\n";
echo "<size>".bytesToSize($row['size'])."</size>\n";
echo "</apistat>\n";
}
$taccf = number_format($tacc);
$tbw = bytesToSize($tsize);
echo <<<EOT
<tcall>$taccf</tcall>
<tbw>$tbw</tbw>
</apistats>
EOT;
          exit();
     }
}


// print an error in the eve-api error style
function print_error($str) {
    print "<?xml version='1.0' encoding='UTF-8'?>\n";
    print "<eveapi version=\"2\">\n";
    print "<currentTime>".date('Y-m-d H:i:s')."</currentTime>\n";
    print "<error code=\"9999\">eproxy error: $str</error>\n";
    print "<cachedUntil>".date('Y-m-d H:i:s')."</cachedUntil>\n";
    print "</eveapi>\n";
}

function exception_handler($exception) {
    print_error("Uncaught exception: " . $exception->getMessage());
    exit();
}

set_exception_handler('exception_handler');

if (array_key_exists("PATH_INFO", $_SERVER) && $_SERVER['PATH_INFO']) {
    $pathinfo = $_SERVER['PATH_INFO'];
} else {
    // try to emulate PATH_INFO; courtesy of Shinhan
    // find the first '/' after the first '.' -- this won't work if, for some reason, the path
    // to eproxy.php has a directory component with a period in it beforehand.  You can
    // fix that by changing the '.' to the script name, e.g. 'eproxy.php'
    $start = strpos($_SERVER['REQUEST_URI'],'/',strpos($_SERVER['REQUEST_URI'],'.'));
    $end = strpos($_SERVER['REQUEST_URI'],'?');
    if ($end > 0) {
        // we've got a GET query string at the end
        $pathinfo = substr($_SERVER['REQUEST_URI'],$start,$end - $start);
    } else {
        $pathinfo = substr($_SERVER['REQUEST_URI'],$start);
    }
}

$apicall = preg_replace('/[^A-Za-z0-9\/.-]/', '', $pathinfo);
//with user
$amat = explode('/', $apicall);
$apiuser = "anon";
if(count($amat) == 3){
    $apicall = '/' . $amat[1] . '/' . $amat[2];
}elseif(count($amat) == 4){
    $apiuser = $amat[1];
    $apicall = '/' . $amat[2] . '/' . $amat[3];
}else{
    print_error("API Path Error. Contact developer.");
    ob_flush();
    return;
}
//end with user
if($enableanalytics){
    getXSL($apicall, $apiuser);
}

// Which arguments each api call takes
$apiArray = array(
	"/account/APIKeyInfo.xml.aspx" => array(
			"interval" => 300,
			"required" => array("keyID", "vCode"),
			"optional" => array()),
	"/account/AccountStatus.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode"),
			"optional" => array()),
	"/account/Characters.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode"),
			"optional" => array()),
	"/char/AccountBalance.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/AssetList.xml.aspx" => array(
			"interval" => 21600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array("version")),
	"/char/CalendarEventAttendees.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID", "eventIDs"),
			"optional" => array()),
	"/char/CharacterSheet.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/ContactList.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/ContactNotifications.xml.aspx" => array(
			"interval" => 21600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/Contracts.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array("contractID")),
	"/char/ContractBids.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/ContractItems.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID", "contractID"),
			"optional" => array()),
	"/char/FacWarStats.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/IndustryJobs.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/KillLog.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array("beforeKillID")),
	"/char/MailBodies.xml.aspx" => array(
			"interval" => 1800,
			"required" => array("keyID", "vCode", "characterID", "ids"),
			"optional" => array()),
	"/char/MailingLists.xml.aspx" => array(
			"interval" => 21600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/MailMessages.xml.aspx" => array(
			"interval" => 1800,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/MarketOrders.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array("orderID")),
	"/char/Medals.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/Notifications.xml.aspx" => array(
			"interval" => 1800,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/NotificationTexts.xml.aspx" => array(
			"interval" => 1800,
			"required" => array("keyID", "vCode", "characterID", "IDs"),
			"optional" => array()),
	"/char/Research.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/SkillInTraining.xml.aspx" => array(
			"interval" => 300,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/SkillQueue.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/Standings.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/UpcomingCalendarEvents.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/WalletJournal.xml.aspx" => array(
			"interval" => 1620,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array("fromID", "rowCount")),
	"/char/WalletTransactions.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array("fromID", "rowCount")),
	"/char/Contracts.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/char/Locations.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID", "IDs"),
			"optional" => array()),
	"/corp/AccountBalance.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/AssetList.xml.aspx" => array(
			"interval" => 21600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array("version")),
	"/corp/MemberMedals.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/CorporationSheet.xml.aspx" => array(
			"interval" => 21600,
			"required" => array("corporationID"),
			"optional" => array("keyID", "vCode")),
	"/corp/ContactList.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/ContainerLog.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/FacWarStats.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/IndustryJobs.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/KillLog.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array("beforeKillID")),
	"/corp/MemberSecurity.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/MemberSecurityLog.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/MarketOrders.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array("orderID")),
	"/corp/Medals.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/OutpostList.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/OutpostServiceDetail.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID", "itemID"),
			"optional" => array()),
	"/corp/Shareholders.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/StarbaseDetail.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "itemID"),
			"optional" => array()),
	"/corp/Standings.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/StarbaseList.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode"),
			"optional" => array()),
	"/corp/WalletJournal.xml.aspx" => array(
			"interval" => 1620,
			"required" => array("keyID", "vCode", "characterID", "accountKey"),
			"optional" => array("fromID", "rowCount")),
	"/corp/WalletTransactions.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID", "accountKey"),
			"optional" => array("fromID", "rowCount")),
	"/corp/Titles.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/Contracts.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array("contractID")),
	"/corp/ContractBids.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/corp/ContractItems.xml.aspx" => array(
			"interval" => 900,
			"required" => array("keyID", "vCode", "characterID", "contractID"),
			"optional" => array()),
	"/corp/Locations.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID", "IDs"),
			"optional" => array()),
	"/corp/MemberTracking.xml.aspx" => array(
			"interval" => 21600,
			"required" => array("keyID", "vCode"),
			"optional" => array("extended")),
	"/eve/AllianceList.xml.aspx" => array(
			"interval" => 3600,
			"required" => array(),
			"optional" => array()),
	"/eve/CertificateTree.xml.aspx" => array(
			"interval" => 86400,
			"required" => array(),
			"optional" => array()),
	"/eve/CharacterID.xml.aspx" => array(
			"interval" => 86400,
			"required" => array("names"),
			"optional" => array()),
	"/eve/CharacterName.xml.aspx" => array(
			"interval" => 86400,
			"required" => array("ids"),
			"optional" => array()),
	"/eve/ConquerableStationList.xml.aspx" => array(
			"interval" => 3600,
			"required" => array(),
			"optional" => array()),
	"/eve/ErrorList.xml.aspx" => array(
			"interval" => 86400,
			"required" => array(),
			"optional" => array()),
	"/eve/FacWarStats.xml.aspx" => array(
			"interval" => 3600,
			"required" => array(),
			"optional" => array()),
	"/eve/FacWarTopStats.xml.aspx" => array(
			"interval" => 3600,
			"required" => array(),
			"optional" => array()),
	"/eve/RefTypes.xml.aspx" => array(
			"interval" => 86400,
			"required" => array(),
			"optional" => array()),
	"/eve/SkillTree.xml.aspx" => array(
			"interval" => 86400,
			"required" => array(),
			"optional" => array()),
	"/eve/CharacterInfo.xml.aspx" => array(
			"interval" => 3600,
			"required" => array("keyID", "vCode", "characterID"),
			"optional" => array()),
	"/eve/TypeName.xml.aspx" => array(
			"interval" => 86400,
			"required" => array("ids"),
			"optional" => array()),
	"/map/FacWarSystems.xml.aspx" => array(
			"interval" => 3600,
			"required" => array(),
			"optional" => array()),
	"/map/Jumps.xml.aspx" => array(
			"interval" => 3600,
			"required" => array(),
			"optional" => array()),
	"/map/Kills.xml.aspx" => array(
			"interval" => 3600,
			"required" => array(),
			"optional" => array()),
	"/map/Sovereignty.xml.aspx" => array(
			"interval" => 3600,
			"required" => array(),
			"optional" => array()),
	"/map/SovereigntyStatus.xml.aspx" => array(
			"interval" => 3600,
			"required" => array(),
			"optional" => array()),
	"/server/ServerStatus.xml.aspx" => array(
			"interval" => 180,
			"required" => array(),
			"optional" => array()),
	"/api/calllist.xml.aspx" => array(
			"interval" => 86400,
			"required" => array(),
			"optional" => array()),
);

function encryptionKey($api, $params) {
    //global $APIArgs;
    global $apiArray;

    $i = array_search("vCode", $apiArray[$api]['required']);
    if ($i === NULL || $i === FALSE)
        return null;
    return base64_decode($params["vCode"]);
}

function extractArgs($api, $params) {
    //global $APIArgs;
    //global $OptionalAPIArgs;
    global $apiArray;

    $k = array();

    foreach ($apiArray[$api]['required'] as $a) {
        if (!array_key_exists($a, $params)) {
            print_error("Required argument $a missing");
            exit();
        }

        $k[$a] = $params[$a];
        
    }

    //if (array_key_exists($api, $OptionalAPIArgs)) {
        foreach ($apiArray[$api]['optional'] as $a) {
            if (array_key_exists($a, $params))
                $k[$a] = $params[$a];
        }
    //}

    return $k;
}

function decryptData($data, $key) {
    global $do_crypt;
    if(!$do_crypt || $key == null || substr_compare($data, "<?xml", 0) == 0)
        return $data;

    $data = base64_decode($data);
    $mod = mcrypt_module_open('blowfish', '', 'cfb', '');

    $iv = substr($data, 0, mcrypt_enc_get_iv_size($mod));
    $cdata = substr($data, mcrypt_enc_get_iv_size($mod));

    mcrypt_generic_init($mod, $key, $iv);
    $decrypted_data = mdecrypt_generic($mod, $cdata);
    mcrypt_generic_deinit($mod);
    mcrypt_module_close($mod);

    return gzuncompress($decrypted_data);
}

function encryptData($data, $key) {
    global $do_crypt;
    if(!$do_crypt || $key == null)
        return $data;

    $mod = mcrypt_module_open('blowfish', '', 'cfb', '');
    $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($mod));
    mcrypt_generic_init($mod, $key, $iv);
    $encrypted_data = mcrypt_generic($mod, gzcompress($data));
    mcrypt_generic_deinit($mod);
    mcrypt_module_close($mod);

    return base64_encode($iv . $encrypted_data);
}

function makeKey($api, $params) {
    //global $APIArgs;
    global $apiArray;

    $k = "";

    // make the key into a concatenation of the param values
    // as listed in the APIArgs array
    foreach ($apiArray[$api]['required'] as $a) {
        if (!array_key_exists(strtolower($a), array_change_key_case($params))) {
            print_error("Missing parameter: " . $a);
            exit();
        }
        $k .= preg_replace('/[^A-Za-z0-9-]/', '', $params[$a]);
    }

    // take the SHA-256 hash
    if ($k == "")
        return "";
    return hash('sha256', $k);
}

if (!array_key_exists($apicall, $apiArray)) {
    print_error("Unknown api call: $apicall");
    ob_flush();
    return;
}

eproxy_db_connect();

$args = extractArgs($apicall, $_REQUEST);
$key = makeKey($apicall, $args);
$cryptkey = encryptionKey($apicall, $args);

// grab a cached value from the database, if any
$value = eproxy_db_query($apicall, $key);

// did we find a cached value?
if ($value != null) {
    $data = decryptData($value, $cryptkey);
    print $data;
    ob_flush();
    if($enableanalytics){
        eproxy_db_accessed($apicall, strlen($data), $apiuser);
    }
    exit();
}

// no cached value; have to do the actual query

$eve_api = "http://api.eve-online.com" . $apicall;
//print $eve_api . "\n";

$req = curl_init($eve_api);
curl_setopt($req, CURLOPT_POST, true);
curl_setopt($req, CURLOPT_POSTFIELDS, $args);
curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
$resp = curl_exec($req);
$http_code = curl_getinfo($req,  CURLINFO_HTTP_CODE);
curl_close($req);

// check if the response has an error
if ($http_code != 200 || strpos($resp, "<error") !== FALSE) {
    // yeah, there was an error; don't save this
    print $resp;
    ob_flush();
    return;
}

$cachedata = encryptData($resp, $cryptkey);

// save the non-error response into the cache
// XXX parse the cachedUntil and use that instead of our cache timer
eproxy_db_insert($apicall, $key, $cachedata);

print $resp;
ob_flush();
if($enableanalytics){
    eproxy_db_accessed($apicall, strlen($cachedata), $apiuser);
}
?>